#!/bin/sh

build_directory="pages"
thedate=$(date '+%d-%m-%Y:')

# first commit changes in the source repository
git add .
git commit
git push origin master

# delete previous site built, if it exists
if [ -d "$build_directory" ]; then
  echo "Found previous site build, deleting it"
  rm -rf $build_directory
  git clone https://codeberg.org/WitherCubes/pages.git
fi

# generate hugo static site to `pages` directory
hugo --gc --destination $build_directory

cd $build_directory || exit

git add .
git commit "$thedate update to website"
git push origin master
