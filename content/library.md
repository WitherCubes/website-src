---
title: My Personal Library
description: List of books in my personal library.
---

This is a list of the books in my personal library. I haven't read every book here, nor do I own all the books.

The links to the books listed are Amazon India links because they are easy for me to buy or link them to a friend, those are not affiliate links. If you wish to not use Amazon, that is totally fine with me.

## Biography

- [Barack Obama: A Promised Land](https://www.amazon.in/Promised-Land-Barack-Obama/dp/0241491517)
- [Escobar: The Inside Story of Pablo Escobar, the World's Most Powerful Criminal](https://www.amazon.in/Escobar-Inside-Worlds-Powerful-Criminal/dp/0340951109)
- [Steve Jobs: The Exclusive Biography](https://www.amazon.in/Steve-Jobs-Walter-Isaacson/dp/034914043X)

## Chetan Bhagat

- [2 States: The Story Of My Marriage](https://www.amazon.in/2-States-Story-My-Marriage/dp/8129135523)
- [Revolution Twenty 20: Love. Corruption. Ambition](https://www.amazon.in/Revolution-Twenty-20-Corruption-Ambition/dp/8129135531)
- [The 3 Mistakes Of My Life](https://www.amazon.in/3-Mistakes-My-Life/dp/8129135515)

## Dan Brown

- [Angels and Demons](https://www.amazon.in/Angels-Demons-Robert-Langdon-Brown/dp/0552161268)
- [The Da Vinci Code](https://www.amazon.in/Vinci-Code-Robert-Langdon/dp/0552161276)
- [The Lost Symbol](https://www.amazon.in/Lost-Symbol-Robert-Langdon/dp/0552161233)
- [Inferno](https://www.amazon.in/Inferno-Robert-Langdon-Dan-Brown/dp/0552169595)
- [Origin](https://www.amazon.in/Origin-Robert-Langdon-Dan-Brown/dp/0552174165)

## Dante Alighieri

- [The Inferno](https://www.amazon.in/Inferno-Dante-Alighieri/dp/817599388X)

## Luke Rhinehart

- [The Dice Man](https://www.amazon.in/Dice-Man-Luke-Rhinehart/dp/0879518642)

## Max Tegmark

- [Life 3.0: Being Human in the Age of Artificial Intelligence](https://www.amazon.in/Life-3-0-Being-Artificial-Intelligence/dp/0141981806)
- [Our Mathematical Universe: My Quest for the Ultimate Nature of Reality](https://www.amazon.in/Our-Mathematical-Universe-Max-Tegmark/dp/0241954630)
