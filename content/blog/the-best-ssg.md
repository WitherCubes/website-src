---
title: The "Best" Static Site Generator
description: Why Hugo is the "best" Static Site Generator.
date: 2022-07-20T12:23:23+05:30
---

A month ago I switched my personal website to [Hugo](https://gohugo.io). Before finding out about Hugo, I was using [lowdown](https://kristaps.bsd.lv/lowdown). I converted all markdown to HTML using a very simple bash script. I stopped doing it that way because it broke a lot (probably because I am stupid) and I had to do a lot of manual intervention.

## Why not something "simple" like Wordpress or Jekyll?

[Wordpress](https://wordpress.com) feels pretty bloated for my use case. Plus there are **a lot** of [security vulnerabilities](https://www.cvedetails.com/vulnerability-list/vendor_id-2337/product_id-4096/Wordpress-Wordpress.html) and exploits which are still not known and hard to mitigate. I would not recommend anyone to use Wordpress for any serious work.

[Jekyll](https://jekyllrb.com) is pretty good but it does not give me control and more features unlike Hugo.[^1] Jekyll feels kind of slow compared to other static site generators. For some people Jekyll is a great choice but it is sadly not for me.

[^1]: Out of the box Jekyll has less features compared to Hugo but it is possible to extend it to your liking.

## Why not use JabaSkript crap?

I hate people using JavaScript to build websites. It looks **_cool_** but it is super slow. It takes up a lot of bandwidth with worse loading times. I do not hate JavaScript when it is used in the right situations (for banking, games, etc). I do not like it when whole websites are made in JavaScript when it can be made very minimally in HTML and some JavaScript.

But if you do not care whatsoever and you love JavaScript or if you have a valid use case to build your website using JavaScript, [Next.js](https://nextjs.org) and [Nuxt.js](https://nuxtjs.org) are great options.

## Why Hugo is the "best" Static Site Generator?

There are other good static site generators like [Zola](https://www.getzola.org) but they are not as well documented as Hugo. Moreover there are many online guides for Hugo to help you out. Hugo is feature rich and its templates are amazing. It fulfills all my needs and I am happy with it.

Check out the [Hugo docs](https://gohugo.io/documentation). They are a great resource to learn a lot about Hugo and master it. There is also a series of videos for Hugo made by [Mike Dane](https://youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3), [Luke Smith](https://youtube.com/playlist?list=PL-p5XmQHB_JT3UT9q9vqhRz246ZNzlXcX) and [Eric Murphy](https://youtube.com/playlist?list=PLnur5_dvCveF8HvYJAjdKLLm1De1Js_Nr).

## What are the alternatives to Hugo?

If you don't want to use use Hugo for some reason, check out [Zola](https://www.getzola.org). It is similar to Hugo but it is written in Rust. If you want to go even more minimalistic and you just want to convert your markdown to HTML and write your own scripts to tailor it to your needs, [lowdown](https://kristaps.bsd.lv/lowdown), [smu](https://github.com/Gottox/smu) and [md4c](https://github.com/mity/md4c) are great options.

If you are interested in making your own personal website but have no experience, you can learn the basics from [LandChad.net](https://landchad.net).
